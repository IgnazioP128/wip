<?php

abstract class PDOconnection {
    
    public const insert = "INSERT INTO wip.";
    public const values = " VALUES(";
    public const select = "SELECT ";
    public const selall= "*";
    public const from = " FROM wip.";
    public const where = "WHERE(";
    
    public function __construct() {
        return $this->connessione();
    }


    private function connessione(){
        try{
            $dsn = "mysql://localhost:3306;dbname=wip";
            $PDOconn = new PDO($dsn, "root", "");
            $PDOconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $PDOconn;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function doQuery(string $value) {
        try{
            $st = $this->connessione()->query($value);
            return $st;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    function queryInsertGeneric(string $table, string $columns, string $value) : string{
        return $this::insert . $table . $columns . $this::values . $value . ")";
    }

    function getQuerySelect($seltabel){
        return $this->doQuery($this::select . $this::selall . $this::from . $seltabel);
    }

    abstract function insert($value);
}
?>