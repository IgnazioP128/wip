<?php

include_once 'Utility/CommentiUtility.php';
include_once 'Utility/PostUtility.php';

class Controller {
    function selectcontroller(){
        if(isset($_POST['cidpost']) && isset($_POST['ctesto'])) {
            $_commentiUtility = new CommentiUtility();
            $_commentiUtility->insertData();
        } else {
            if(isset($_POST['ptesto'])) {
                $_postUtility = new PostUtility();
                $_postUtility->insertData();
            }
            else {
                header("Location: ../View/error.html", true, 400);
                exit();
            }
        }
    }
}

$controller = new Controller();
$controller->selectcontroller();

?>