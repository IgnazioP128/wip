<?php

include_once '../Model/Commenti.php';

class CommentiUtility {
    
    public function insertData(){
        $_testo = $_POST['ctesto']; 
        $_idPost = $_POST['cidpost'];
        $model = new Commenti();
        $model->setTesto($_testo);
        $model->setIdPost($_idPost);
        $model->insert($model->genericInsertArray());
        header("Location: ../View/view.php", true, 301);
        exit();
    }
}