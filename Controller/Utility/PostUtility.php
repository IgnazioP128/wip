<?php

include_once '../Model/Post.php';

class PostUtility {
    
    public function insertData(){
        $_testo = $_POST['ptesto']; 
        $model = new Post();
        $model->setTesto($_testo);
        $model->insert($model->genericInsertArray());
        header("Location: ../View/view.php", true, 301);
        exit();
    }
}