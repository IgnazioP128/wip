<?php 
include_once '../Model/Post.php';
include_once '../Model/Commenti.php';


$_postModel = new Post();
$_commentiModel = new Commenti();

?>
<!DOCTYPE html>
<html lang="it-IT">
    <head>
        <title>Wip</title>
    </head>
    <body>
        <form id="mform" action="/wip/controller/controller.php" method="post">
            <div class="container">
            <h1>Scrivi un post</h1>
            <label for="ptesto"><b>Testo</b></label>
            <input type="text" placeholder="Inserisci testo" name="ptesto" id="ptesto" required>
            <br>
            <button type="submit" class="postbtn">Crea</button>
            </div>
        </form>
        <hr>
        <?php 
            foreach($_postModel->selectStar() as $record) {
            ?>
            <h2><?php echo $record["testo"]; ?></h2>
            <h3>commenti:</h3>
            <?php
                foreach($_commentiModel->selectPerId($record["idpost"]) as $recordc) { ?>
                    <p><?php echo $recordc["testo"]; ?></p>
            <?php } ?>
            <hr>
            <form id="cform" action="/wip/controller/controller.php" method="post">
                <div class="container">
                    <input type="hidden" id="cidpost" name="cidpost" value="<?php echo $record["idpost"];?>" />
                <p>scrivi un commento!</p>
            
                <label for="ctesto"><b>Testo</b></label>
                <input type="text" placeholder="Inserisci commento" name="ctesto" id="ctesto" required>
                <br>
                <button type="submit" class="commentobtn">Commenta!</button>
                </div>
            </form>

            <hr>
            <?php } ?>

    </body>
</html>