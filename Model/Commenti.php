<?php

include_once '../PDO/PDOconnection.php';

class Commenti extends PDOconnection {

    const tab = "commenti";
    const colonne = " (testo, idpost)";

    protected $_testo;
    protected $_idpost;

    function getTesto() : string {
        return $this->_testo;
    }
    function setTesto(string $testo){
        $this->_testo = $testo; 
    }

    function getIdPost() : string {
        return $this->_idpost;
    }
    function setIdPost(string $idpost){
        $this->_idpost = $idpost; 
    }
    
    public function selectStar() : array{
        return $this->getQuerySelect(Commenti::tab)->fetchAll();
    }

    public function selectPerId($id) : array{
        $c = [];
        foreach($this->getQuerySelect(Commenti::tab)->fetchAll() as $record){
            if($record["idpost"] == $id) {
                $c[] = $record;
            }
        }
        return $c;
    }

    function genericInsertArray() : string {
        if($this->getTesto() != NULL && $this->getIdPost() != NULL){
            return "'{$this->getTesto()}', {$this->getIdPost()}";
        } else {
            throw new Exception("dati mancanti");
        }
    }

    function insert($value){
        return $this->doQuery($this->queryInsertGeneric($this::tab, $this::colonne, $value));
    }
}
?>