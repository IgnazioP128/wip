<?php

include_once '../PDO/PDOconnection.php';

class Post extends PDOconnection {

    const tab = "post";
    const colonne = " (testo)";

    protected $_testo;

    function getTesto() : string {
        return $this->_testo;
    }
    function setTesto(string $testo){
        $this->_testo = $testo; 
    }

    public function selectStar() : array{
        return $this->getQuerySelect(Post::tab)->fetchAll();
    }

    function genericInsertArray() : string {
        if($this->getTesto() != ""){
            return "'{$this->getTesto()}'";
        } else {
            throw new Exception("dati mancanti");
        }
    }

    function insert($value){
        return $this->doQuery($this->queryInsertGeneric($this::tab, $this::colonne, $value));
    }
}
?>